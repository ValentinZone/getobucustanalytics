package com.dna.ae.controllers;


import com.dna.ae.domain.ObuDataDto;
import com.googlecode.jsonrpc4j.JsonRpcParam;
import com.googlecode.jsonrpc4j.JsonRpcService;

import java.util.List;

@JsonRpcService("/GetobuIdAnalytics")
public interface CustObuServiceController {

    List<ObuDataDto> readData();

    List<ObuDataDto> readData(@JsonRpcParam(value = "obuId") String obuId);

    List<ObuDataDto> readDataCust(@JsonRpcParam(value = "customerId") String customerId);

    List<ObuDataDto> readDataMonth(@JsonRpcParam(value = "month") Integer month);

    List<ObuDataDto> readDataYear(@JsonRpcParam(value = "year") Integer year);

    List<ObuDataDto> readDataRegion(@JsonRpcParam(value = "region") Integer region);

    List<ObuDataDto> readDataBilled(@JsonRpcParam(value = "billed") Boolean billed);

    List<ObuDataDto> readData(@JsonRpcParam(value = "obuId") String obuId,
                              @JsonRpcParam(value = "customerId") String customerId);

    List<ObuDataDto> readData(@JsonRpcParam(value = "obuId") String obuId,
                              @JsonRpcParam(value = "billed") Boolean billed);


    List<ObuDataDto> readData(@JsonRpcParam(value = "obuId") String obuId,
                              @JsonRpcParam(value = "month") Integer month,
                              @JsonRpcParam(value = "year") Integer year);


}
