package com.dna.ae.repositories;

import com.dna.ae.services.reposervices.ObuDataDao;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ObuDataRepository extends JpaRepository<ObuDataDao, Integer> {

    ObuDataDao save(ObuDataDao obuDataDao);

    public List<ObuDataDao> findAllByObuId(String obuId);

    public List<ObuDataDao> findAllByCustomerId(String customerId);

    public List<ObuDataDao> findAllByMonth(Integer month);

    public List<ObuDataDao> findAllByYear(Integer year);

    public List<ObuDataDao> findAllByRegion(Integer region);

    public List<ObuDataDao> findAllByBilled(Boolean billed);

    public List<ObuDataDao> findByObuIdAndCustomerId(String obuId, String customerId);

    public List<ObuDataDao> findByObuIdAndBilled(String obuId, Boolean billed);

    public List<ObuDataDao> findByObuIdAndMonthAndYear(
            String obuId, Integer month, Integer year);

}