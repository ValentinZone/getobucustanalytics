package com.dna.ae.services.jpaservice;

import com.dna.ae.domain.ObuDataDto;
import com.dna.ae.repositories.ObuDataRepository;
import com.dna.ae.services.ObuService;
import com.dna.ae.services.reposervices.ObuDataDao;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;


@Service
@Transactional
@ComponentScan("com.dna.ae.services.reposervices.ObuDataDao")
public class ObuServiceImpl implements ObuService {

    private static final Logger logger = Logger.getLogger(ObuServiceImpl.class);
    private final ObuDataRepository obuDataRepository;

    @Autowired
    public ObuServiceImpl(ObuDataRepository obuDataRepository) {
        this.obuDataRepository = obuDataRepository;
    }


    @Override
    public List<ObuDataDto> read() {
        List<ObuDataDao> temp = obuDataRepository.findAll();
        List<ObuDataDto> result = fillDtoWithDao(temp);
        return result;
    }

    @Override
    public List<ObuDataDto> read(Integer type, String info) {
        List<ObuDataDao> temp = new ArrayList<>();
        switch (type) {
            case 0:
                temp = obuDataRepository.
                        findAllByObuId(info);
                break;
            case 1:
                temp = obuDataRepository.
                        findAllByCustomerId(info);
                break;
        }
        List<ObuDataDto> result = fillDtoWithDao(temp);
        return result;
    }

    @Override
    public List<ObuDataDto> read(Integer type, Integer info) {
        List<ObuDataDao> temp = new ArrayList<>();
        switch (type) {
            case 0:
                temp = obuDataRepository.
                        findAllByMonth(info);
                break;
            case 1:
                temp = obuDataRepository.
                        findAllByYear(info);
                break;
            case 2:
                temp = obuDataRepository.
                        findAllByRegion(info);
        }
        List<ObuDataDto> result = fillDtoWithDao(temp);
        return result;
    }

    @Override
    public List<ObuDataDto> read(Boolean billed) {
        List<ObuDataDao> temp = obuDataRepository.
                findAllByBilled(billed);
        List<ObuDataDto> result = fillDtoWithDao(temp);
        return result;
    }

    @Override
    public List<ObuDataDto> read(String obuId, String customerId) {

        List<ObuDataDao> temp = obuDataRepository.
                findByObuIdAndCustomerId(obuId, customerId);
        List<ObuDataDto> result = fillDtoWithDao(temp);
        return result;
    }

    @Override
    public List<ObuDataDto> read(String obuId, Integer month, Integer year) {
        List<ObuDataDao> temp = obuDataRepository.
                findByObuIdAndMonthAndYear(obuId, month, year);
        List<ObuDataDto> result = fillDtoWithDao(temp);
        return result;
    }

    @Override
    public List<ObuDataDto> read(String obuId, Boolean billed) {
        List<ObuDataDao> temp = obuDataRepository.
                findByObuIdAndBilled(obuId, billed);
        List<ObuDataDto> result = fillDtoWithDao(temp);
        return result;
    }

    private List<ObuDataDto> fillDtoWithDao(List<ObuDataDao> temp) {

        List<ObuDataDto> result = new ArrayList<>();
        for (ObuDataDao obuDataDao : temp) {
            ObuDataDto obuDataDto = new ObuDataDto();
            obuDataDto.setObuId(obuDataDao.getObuId());
            obuDataDto.setCustomerId(obuDataDao.getCustomerId());
            obuDataDto.setMonth(obuDataDao.getMonth());
            obuDataDto.setYear(obuDataDao.getYear());
            obuDataDto.setAmount(obuDataDao.getAmount());
            obuDataDto.setDistance(obuDataDao.getDistance());
            obuDataDto.setRegion(obuDataDao.getRegion());
            obuDataDto.setBilled(obuDataDao.getBilled());
            obuDataDto.setId(obuDataDao.getId());

            result.add(obuDataDto);
        }
        return result;
    }
}
