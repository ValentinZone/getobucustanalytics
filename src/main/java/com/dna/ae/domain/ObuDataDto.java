package com.dna.ae.domain;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Data

public class ObuDataDto implements Serializable {
    @NotNull
    private Integer id;

    private String obuId;

    private String customerId;
    private Integer month;
    private Integer year;
    private Double amount;
    private Integer distance;
    private Integer region;
    private Boolean billed;


}
