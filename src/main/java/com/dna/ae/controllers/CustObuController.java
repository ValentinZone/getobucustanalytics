package com.dna.ae.controllers;

import com.dna.ae.domain.ObuDataDto;
import com.dna.ae.services.ObuService;
import com.googlecode.jsonrpc4j.spring.AutoJsonRpcServiceImpl;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AutoJsonRpcServiceImpl
public class CustObuController implements CustObuServiceController {

    private static final Logger logger = Logger.getLogger(CustObuController.class);
    private final ObuService obuService;

    @Autowired
    public CustObuController(ObuService obuService) {
        this.obuService = obuService;
    }

    @Override
    public List<ObuDataDto> readData() {
        return obuService.read();
    }

    @Override
    public List<ObuDataDto> readData(String obuId) {
        List<ObuDataDto> ans = obuService.read(0, obuId);
        return ans;
    }

    @Override
    public List<ObuDataDto> readDataCust(String customerId) {
        List<ObuDataDto> ans = obuService.read(1, customerId);
        return ans;
    }

    @Override
    public List<ObuDataDto> readDataMonth(Integer month) {
        List<ObuDataDto> ans = obuService.read(0, month);
        return ans;
    }

    @Override
    public List<ObuDataDto> readDataYear(Integer year) {
        List<ObuDataDto> ans = obuService.read(1, year);
        return ans;
    }

    @Override
    public List<ObuDataDto> readDataRegion(Integer region) {
        List<ObuDataDto> ans = obuService.read(2, region);
        return ans;
    }

    @Override
    public List<ObuDataDto> readDataBilled(Boolean billed) {
        List<ObuDataDto> ans = obuService.read(billed);
        return ans;
    }

    @Override
    public List<ObuDataDto> readData(String obuId, String customerId) {
        List<ObuDataDto> ans = obuService.read(obuId, customerId);
        return ans;
    }

    @Override
    public List<ObuDataDto> readData(String obuId, Boolean billed) {
        List<ObuDataDto> ans = obuService.read(obuId, billed);
        return ans;
    }

    @Override
    public List<ObuDataDto> readData(String obuId, Integer month, Integer year) {
        List<ObuDataDto> ans = obuService.read(obuId, month, year);
        return ans;
    }
}
