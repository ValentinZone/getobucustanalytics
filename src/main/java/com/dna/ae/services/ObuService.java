package com.dna.ae.services;

import com.dna.ae.domain.ObuDataDto;

import java.util.List;

public interface ObuService {

    List<ObuDataDto> read();

    List<ObuDataDto> read(Integer type, String info);

    List<ObuDataDto> read(Integer type, Integer info);

    List<ObuDataDto> read(Boolean billed);

    List<ObuDataDto> read(String obuId, String customerId);

    List<ObuDataDto> read(String obuId, Integer month, Integer year);

    List<ObuDataDto> read(String obuId, Boolean billed);
}
