package com.dna.ae.services.reposervices;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity

@Table(name = "SAMPLE_DATA")
public class ObuDataDao {

    @Column(name = "OBU_ID")
    private String obuId;

    @Column(name = "CUSTOMER_ID")
    private String customerId;

    @Column(name = "MONTH")
    private Integer month;

    @Column(name = "YEAR")
    private Integer year;

    @Column(name = "AMOUNT", precision = 16, scale = 2)
    private Double amount;

    @Column(name = "DISTANCE")
    private Integer distance;

    @Column(name = "REGION")
    private Integer region;

    @Column(name = "BILLED")
    private Boolean billed;

    @Id
    @Column(name = "ID")
    private Integer id;
}
