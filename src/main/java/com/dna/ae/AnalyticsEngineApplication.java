package com.dna.ae;

import org.apache.log4j.BasicConfigurator;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AnalyticsEngineApplication {

	public static void main(String[] args) {
		BasicConfigurator.configure();
		SpringApplication.run(AnalyticsEngineApplication.class, args);
	}

}
